<?php

namespace App\Filters;

use Config\Services;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use App\Models\UserModel;
use CodeIgniter\Commands\Server\Serve;
use CodeIgniter\I18n\Time;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Exception;

class AuthFilter implements FilterInterface
{
    protected $userModel;
    protected $secretKey;

    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->secretKey = Services::getSecretKey();
    }

    public function before(RequestInterface $request, $arguments = null)
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");

        $userModel = $this->userModel;
        $auth_header = $request->getServer('HTTP_AUTHORIZATION');

        if ($auth_header) {
            $secret_key = $this->secretKey;
            $explode_white_space = explode(' ', $auth_header);
            $token = $explode_white_space[1];

            if ($this->CheckExpiredToken($token)) {
                try {
                    $decode_jwt = JWT::decode($token, new Key($secret_key, 'HS256'));
                    $validateUser = $userModel->validateUser($decode_jwt->email);

                    if ($validateUser) {
                        $request->user = $validateUser;
                        return $request;
                    }

                    return Services::response()
                        ->setStatusCode('401')
                        ->setJSON([
                            'status' => 401,
                            'messages' => 'Cannot Acces This Resources! Invalid Users !'
                        ]);
                } catch (Exception $e) {
                    return Services::response()->setStatusCode(404)->setJSON([
                        'status' => 404,
                        'message' => $e->getMessage()
                    ]);
                }
            } else {
                return Services::response()
                    ->setStatusCode(404)
                    ->setJSON([
                        'status' => 404,
                        'message' => 'Invalid Token ! Expired Token !'
                    ]);
            }
        } else {
            return Services::response()
                ->setStatusCode(403)
                ->setJSON([
                    'status' => 403,
                    'messages' => 'Cannot Acces This Resources! Unauthorize Users !'
                ]);
        }
    }

    /**
     * Allows After filters to inspect and modify the response
     * object as needed. This method does not allow any way
     * to stop execution of other after filters, short of
     * throwing an Exception or Error.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array|null        $arguments
     *
     * @return ResponseInterface|void
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        //
    }

    public function CheckExpiredToken($token)
    {
        $time = new Time();
        $decodeJwt = JWT::decode($token, new Key($this->secretKey, 'HS256'));
        $timeNow = strtotime($time->now('Asia/Jakarta', 'id_ID'));

        return $decodeJwt->expire_at > $timeNow ? true : false;
    }
}
