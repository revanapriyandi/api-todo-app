<?php

namespace App\Commands;

use CodeIgniter\CLI\CLI;
use App\Models\TaskModel;
use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\Session\Session;
use Exception;

class UpdateStatusTasks extends BaseCommand
{
    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'Tasks';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'tasks:update-status';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Update status tasks';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'tasks:update-status';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [];

    /**
     * Actually execute a command.
     *
     * @param array $params
     */
    public function run(array $params)
    {
        try {
            $model = new TaskModel();

            $tasks = $model->where('status', 'in_progress')
                ->where('due_date <=', date('Y-m-d H:i'))
                ->findAll();

            if (empty($tasks)) {
                log_message('info', 'Command tasks:update-status executed without any task found');
                CLI::write('No task found');
                return;
            }

            foreach ($tasks as $task) {
                $model->update($task['task_id'], ['status' => 'overdue']);
                log_message('info', sprintf('Task with id %d updated', $task['task_id']));
                CLI::write(sprintf('Task with task_id %d updated', $task['task_id']));
            }
        } catch (Exception $e) {
            log_message('error', $e->getMessage());
            CLI::error('Something went wrong, ' . $e->getMessage());

            $_SESSION['cmdStatus'] = 'Failed';
        }
    }
}
