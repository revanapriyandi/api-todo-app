<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class Api extends BaseController
{
    public function index()
    {
        $yamlContent = file_get_contents(APPPATH . './Documentation/openapi.yaml');
        return $this->response->setHeader('Content-Type', 'text/plain')->setBody($yamlContent);
    }
}
