<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class Docs extends BaseController
{
    public function index()
    {
        return view('swagger');
    }
}
