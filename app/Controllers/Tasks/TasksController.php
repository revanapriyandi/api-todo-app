<?php

namespace App\Controllers\Tasks;

use Config\Services;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use App\Models\TaskModel;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class TasksController extends BaseController
{
    use ResponseTrait;

    protected $idUser = null;
    protected $model;

    protected $session;

    public function __construct()
    {
        $this->model = new TaskModel();
    }

    public function index(): ResponseInterface
    {
        $this->getUserId();
        $idUser = $this->idUser;

        $status = $this->request->getVar('status');

        $query = $this->model->where('user_id', $idUser);

        if (!empty($status)) {
            $status = strtolower($status);
            $allowedStatus = ['in_progress', 'overdue', 'done'];

            if (!in_array($status, $allowedStatus)) {
                return $this->fail('Status not allowed, allowed status: in_progress, overdue, done', 400);
            }

            $query->where('status', $status);
        }

        $tasks = $query->findAll();

        return $this->respond($tasks);
    }

    public function create($status = 'in_progress'): ResponseInterface
    {
        $this->getUserId();
        $idUser = $this->idUser;
        $data = (object) $this->request->getJSON();

        $rules = $this->model->validationRules;

        if (!$this->validate($rules)) {
            return $this->failValidationErrors($this->validator->getErrors());
        }
        if (strtotime($data->due_date) <= strtotime(date('Y-m-d'))) {
            return $this->fail('Due date cannot be less than today', 400);
        }

        if (strtotime($data->due_date) < strtotime(date('H:i:s'))) {
            return $this->fail('Time cannot be less than current time', 400);
        }

        try {
            $data = [
                'user_id' => $idUser,
                'title' => $data->title,
                'description' => $data->description,
                'status' => $status,
                'due_date' => $data->due_date
            ];
            $this->model->insert($data);

            return $this->respondCreated(['messages' => 'Task created'], 'Task created');
        } catch (Exception $e) {
            log_message('error', $e->getMessage());
            return $this->fail('Something went wrong', 500);
        }
    }

    public function show($id): ResponseInterface
    {
        $this->getUserId();

        if ($id) {
            $task = $this->model->find($id);
            if (!$task) {
                return $this->failNotFound(sprintf('Task with id %d not found', $id));
            }
            if (!$this->checkOwnership($task)) {
                return $this->failUnauthorized('Unauthorized to access this task');
            }
            return $this->respond($task, ResponseInterface::HTTP_OK, 'Task found');
        }
        return $this->fail('Task id is required', 400);
    }

    public function update($id): ResponseInterface
    {
        $this->getUserId();

        $data = (object) $this->request->getJSON();

        $rules = $this->model->validationRules;

        if (!$this->validate($rules)) {
            return $this->failValidationErrors($this->validator->getErrors());
        }

        if (strtotime($data->due_date) < strtotime(date('Y-m-d'))) {
            return $this->fail('Due date cannot be less than today', 400);
        }

        $task = $this->model->find($id);
        if (!$task) {
            return $this->failNotFound(sprintf('Task with id %d not found', $id));
        }

        if (!$this->checkOwnership($task)) {
            return $this->failUnauthorized('Unauthorized to access this task');
        }

        $data = [
            'title' => $data->title,
            'description' => $data->description,
            'due_date' => $data->due_date
        ];
        try {
            $this->model->update($id, $data);

            return $this->respondUpdated(['messages' => 'Task updated'], 'Task updated');
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            return $this->fail('Something went wrong', 500);
        }
    }

    public function updateStatus($id): ResponseInterface
    {
        $this->getUserId();

        $data = (object) $this->request->getJSON();

        $allowedStatus = ['in_progress', 'overdue', 'done'];
        $newStatus =  strtolower($data->status);
        if (!in_array($newStatus, $allowedStatus)) {
            return $this->fail('Status not allowed, allowed status: in_progress, overdue, done', 400);
        }
        $task = $this->model->find($id);
        if (!$task) {
            return $this->failNotFound(sprintf('Task with id %d not found', $id));
        }

        if (!$this->checkOwnership($task)) {
            return $this->failUnauthorized('Unauthorized to access this task');
        }

        try {
            $this->model->update($id, [
                'status' => $newStatus
            ]);

            return $this->respondUpdated(['messages' => 'Task status updated'], 'Task status updated');
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            return $this->fail('Something went wrong', 500);
        }
    }


    public function destroy($id)
    {
        $this->getUserId();

        if ($id) {
            $task = $this->model->find($id);
            if (!$task) {
                return $this->failNotFound(sprintf('Task with id %d not found', $id));
            }

            if (!$this->checkOwnership($task)) {
                return $this->failUnauthorized('Unauthorized to access this task');
            }

            $this->model->delete($id);
            return $this->respondDeleted(['messages' => 'Task deleted'], 'Task deleted');
        }

        return $this->fail('Task id is required', 400);
    }

    private function getUserId(): void
    {
        $request = service('request');
        $token = $request->getServer('HTTP_AUTHORIZATION');

        $secretKey = Services::getSecretKey();
        $tokenParts = explode(' ', $token);
        $decodedToken = JWT::decode($tokenParts[1], new Key($secretKey, 'HS256'));

        $this->idUser = intval(base64_decode($decodedToken->user_id));
    }

    private function checkOwnership($task): bool
    {
        if ($task) {
            return intval($task['user_id']) === $this->idUser;
        }
        return false;
    }
}
