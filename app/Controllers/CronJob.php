<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class CronJob extends BaseController
{
    use ResponseTrait;

    public function status()
    {
        command('tasks:update-status');

        return $this->respond([
            'status' => 'success',
            'last_usage' => date('Y-m-d H:i')
        ]);
    }
}
