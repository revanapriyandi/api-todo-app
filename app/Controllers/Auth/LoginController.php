<?php

namespace App\Controllers\Auth;

use Config\Services;
use Firebase\JWT\JWT;
use App\Models\UserModel;
use CodeIgniter\I18n\Time;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class LoginController extends BaseController
{
    use ResponseTrait;

    protected $model;

    public function __construct()
    {
        $this->model = new UserModel();
    }

    public function login()
    {
        $credentials = (object) $this->request->getVar();

        $rules = [
            'email' => 'required|valid_email',
            'password' => 'required|min_length[8]'
        ];

        if (!$this->validate($rules)) {
            return $this->fail($this->validator->getErrors());
        }

        $generate_token = $this->generateToken($credentials);

        if ($generate_token) {
            return $this->respond([
                'exp' => $generate_token['expired_at'],
                'token' => $generate_token["access_token"],
                'email' => $generate_token["email"],
                'username' => $generate_token["username"]
            ], ResponseInterface::HTTP_OK);
        } else {
            return  $this->fail('Invalid email or password', ResponseInterface::HTTP_UNAUTHORIZED);
        }
    }

    private function generateToken($credentials)
    {
        $time = new Time();
        $valid_credentials = $this->checkLogin($credentials);

        if ($valid_credentials) {
            $key = Services::getSecretKey();
            $iat = strtotime($time->now('Asia/Jakarta', 'id_ID'));
            $exp_access_token = $iat + 5284000;

            $payload_access_token = [
                'user_id' => $valid_credentials["user_id"],
                'username' => $valid_credentials["username"],
                'email' => $valid_credentials["email"],
                'expire_at' => $exp_access_token
            ];
            $payload_refresh_token = [
                'user_id' => $valid_credentials["user_id"],
                'token_refresh' => true
            ];

            $jwt_refresh_token = JWT::encode($payload_refresh_token, $key, 'HS256');
            $jwt_access_token = JWT::encode($payload_access_token, $key, 'HS256');
            return [
                'access_token' => $jwt_access_token,
                'refresh_token' => $jwt_refresh_token,
                'expired_at' => $exp_access_token,
                'email' => $valid_credentials["email"],
                'username' => $valid_credentials["username"],
            ];
        } else {
            return null;
        }
    }

    private function checkLogin($credentials)
    {
        $userData = $this->model->where('email', $credentials->email)->first();

        if ($userData && password_verify($credentials->password, $userData['password'])) {
            return  [
                'user_id' => base64_encode($userData["user_id"]),
                'username' => $userData["username"],
                'email' => $userData["email"]
            ];
        }

        return false;
    }
}
