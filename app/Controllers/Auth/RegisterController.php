<?php

namespace App\Controllers\Auth;

use App\Models\UserModel;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class RegisterController extends BaseController
{
    use ResponseTrait;

    public function register()
    {
        $rules = [
            'username' => 'required|is_unique[users.username]',
            'email' => 'required|valid_email|is_unique[users.email]',
            'password' => 'required|min_length[8]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])/]',
            'password' => [
                'rules' => 'required|min_length[8]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])/]',
                'errors' => [
                    'regex_match' => lang('Validation.regex_match')
                ]
            ],
            'confirm_password' => 'required|matches[password]'
        ];

        if (!$this->validate($rules)) {
            return $this->failValidationErrors($this->validator->getErrors());
        }

        $data = (object) $this->request->getJSON();

        try {
            $userModel = new UserModel();

            $userModel->insert([
                'username' =>  $data->username,
                'email' =>  $data->email,
                'password' => password_hash($data->password, PASSWORD_BCRYPT),
            ]);

            return $this->respondCreated(['message' => 'Registration successfully']);
        } catch (Exception $e) {
            log_message('error', $e->getMessage());
            return $this->fail('Something went wrong', ResponseInterface::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
