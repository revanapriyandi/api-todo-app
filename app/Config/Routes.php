<?php

use Config\Services;

$routes = Services::routes();


if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

$routes->setDefaultMethod('index');

$routes->group('auth', ['namespace' => 'App\Controllers\Auth'], function ($routes) {
    $routes->post('login', 'LoginController::login');
    $routes->post('register', 'RegisterController::register');
});

$routes->group('tasks', ['namespace' => 'App\Controllers\Tasks', 'filter' => 'auth:user'], function ($routes) {
    $routes->get('/', 'TasksController::index');
    $routes->post('create', 'TasksController::create');
    $routes->get('(:num)', 'TasksController::show/$1');
    $routes->put('(:num)', 'TasksController::update/$1');
    $routes->delete('delete/(:num)', 'TasksController::destroy/$1');
    $routes->put('update-status/(:num)', 'TasksController::updateStatus/$1');
});

$routes->get('/', 'Home::index');
$routes->get('openapi', 'Api::index');
$routes->get('docs', 'Docs::index');

$routes->get('cronjob/status', 'CronJob::status');
