<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\RawSql;
use CodeIgniter\Database\Migration;

class AddDueDateToTasks extends Migration
{
    public function up()
    {
        $this->forge->addColumn('tasks', [
            'due_date' => [
                'type' => 'DATETIME',
                'null' => false,
                'after' => 'status',
                'default' =>  new RawSql('CURRENT_TIMESTAMP'),
            ]
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('tasks', 'due_date');
    }
}
