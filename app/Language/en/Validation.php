<?php

// override core en language system validation or define your own en language validation message
return [
    'regex_match' => 'Password must have at least one uppercase letter.',
    'is_unique' => 'Sorry, the {field} has already been registered.'
];
